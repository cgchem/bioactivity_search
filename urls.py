# URLs and a function required by both other modules

get_aids_url = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/aids/JSON?aids_type=active"
# for use with requests.post - POST "cid=1234,5678,91...."

cids_from_aids_url = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/assay/aid/cids/JSON?cids_type=active"
# gets a JSON list of {AID:1234, CID:[123,456]} objects

get_targets_url = '''https://pubchem.ncbi.nlm.nih.gov/rest/pug/assay/aid/targets/ProteinGI,ProteinName,GeneSymbol,GeneID/JSON'''
# get the protein/gene targets for a given assay by AID

aids_info_url = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/assay/aid/summary/json"
# get the summary info for an assay by AID

aids_desc_url = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/assay/aid/description/json"
# get the description for an assay by AID


