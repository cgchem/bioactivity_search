SDF_XML_QUERY = '''<?xml version="1.0"?>
<!DOCTYPE PCT-Data PUBLIC "-//NCBI//NCBI PCTools/EN" "http://pubchem.ncbi.nlm.nih.gov/pug/pug.dtd">
<PCT-Data>
  <PCT-Data_input>
    <PCT-InputData>
      <PCT-InputData_download>
        <PCT-Download>
          <PCT-Download_uids>
            <PCT-QueryUids>
              <PCT-QueryUids_ids>
                <PCT-ID-List>
                  <PCT-ID-List_db>pccompound</PCT-ID-List_db>
                  <PCT-ID-List_uids>
                    %s
                  </PCT-ID-List_uids>
                </PCT-ID-List>
              </PCT-QueryUids_ids>
            </PCT-QueryUids>
          </PCT-Download_uids>
          <PCT-Download_format value="sdf"/>
          <PCT-Download_compression value="none"/>
          <PCT-Download_use-3d value="false"/>
        </PCT-Download>
      </PCT-InputData_download>
    </PCT-InputData>
  </PCT-Data_input>
</PCT-Data>''' #build and insert query list

SDF_XML_SMALL = '''<PCT-ID-List_uids_E>%s</PCT-ID-List_uids_E>''' #insert CID as string

POLL_QUERY = '''<PCT-Data>
  <PCT-Data_input>
    <PCT-InputData>
      <PCT-InputData_request>
        <PCT-Request>
          <PCT-Request_reqid>%s</PCT-Request_reqid>
          <PCT-Request_type value="status"/>
        </PCT-Request>
      </PCT-InputData_request>
    </PCT-InputData>
  </PCT-Data_input>
</PCT-Data>'''

GENE_QUERY = '''https://pubchem.ncbi.nlm.nih.gov/rest/pug_view/data/gene/%s/JSON/?response_type=display''' #to look up a gene by ID and get name etc

#https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gene&id=672+3096&retmode=xml
#for gene synonyms

GENE_NAMES_QUERY = '''https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gene&id=%s&retmode=json'''

GENEID_NAME_SEARCH = '''https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gene&retmax=10000&term=(%s[Gene%%2FProtein+Name])+AND+%%22homo+sapiens%%22[organism]+AND+%%22current%%20only%%22[Filter]'''