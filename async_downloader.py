import asyncio
import aiohttp
from urls import *

# module with a function for faster downloading of assay data by submitting multiple requests
# in parallel using asyncio. This can be faster for big queries but there is the risk of
# being rate-limited by PubChem if too many requests are submitted at once, 3 seems OK

def divide_up(myls, size):

    """Generator, takes a list 'myls' and yields smaller chunked lists of length 'size'"""

    out = []
    for x in myls:
        out.append(x)
        if len(out) >= size:
            yield out
            out = []
    if out:
        yield out  # make sure we get the last subsection that might be shorter than size
        # but don't just return an empty list


def get_assays_info(myls, connection_limit=4):

    """Takes a list of AssayIDs and gathers summary info by submitting a bunch of parallel asyncio requests.
    connection_limit can be increased to parallelize downloads at the risk of getting rate-limited
    by PubChem if too many requests are submitted at once."""

    async def fetch(mylist, session, number):

        to_post = {"aid":",".join([str(x) for x in mylist])}
        # the aiohttp post method works differently to requests.post
        # rather than post the text "aid=1,2,3,4" a dictionary is passed to the method that then formats the POST
        async with session.post(aids_info_url, data=to_post) as response:
            print(f"starting download thread #{number}...")
            return await response.json()

    async def dlmany(alist, container, cli):

        tasks = []
        async with cli as session:
            for h, i in enumerate(divide_up(alist, 200)):
                tsk = asyncio.ensure_future(fetch(i,session,h))
                tasks.append(tsk)
            responses = await asyncio.gather(*tasks)
            for x in responses:
                theinfo = x["AssaySummaries"]["AssaySummary"]
                # look up the actual assay data, changes per function
                container.extend(theinfo)
            print("async download of assay data done")

    print(f"downloading info for {len(myls)} assays (parallel)")
    connector = aiohttp.TCPConnector(limit=connection_limit)
    # limit simultaneous connections to PubChem
    client = aiohttp.ClientSession(connector=connector)
    loop = asyncio.get_event_loop()
    res = []
    fut = asyncio.ensure_future(dlmany(myls,res,client))
    try:
        loop.run_until_complete(fut)
        return res
    except aiohttp.client_exceptions.ServerDisconnectedError:
        raise Exception("async connection lost, try downloading using serial mode "
                        "or decrease the connection limit")
