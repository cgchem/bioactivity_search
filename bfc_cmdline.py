# module to run the functions from the command line instead of interactive python prompt

import argparse
import bioactivity_from_compound as bfc
import sys

parser = argparse.ArgumentParser(description="download assay data from pubchem from compound CID")
parser.add_argument("--cidfile", metavar="cid file", type=str, help="file containing a list of CIDS")
parser.add_argument("--percid", dest="percid", action="store_const", const=1, default=0,
                    help="write separate files for each CID rather than a combined file")
parser.add_argument("--batchsize", metavar="batch size", type=int,
                    help = "how many experiments to download at once, decrease"
                           " if you are being rate-limited by PubChem")
parser.add_argument("--dlall", dest="dlall", action="store_const", const=1, default=0,
                    help = "download all information including assay type")
parser.add_argument("--cidlist", metavar="cid list", type=str,
                    help="provide comma-delimited CID list instead of a file")

args = parser.parse_args()


fnargs = {}

if args.percid == 1:
    fn = bfc.cidstoexcel_percid
else:
    fn = bfc.cidstoexcel

if args.cidlist:
    clist = args.cidlist.split(",")
    for x in clist:
        if len(x) > 10:
            # presumably the splitting has gone wrong
            print("CID list must be a comma-separated list of CIDs e.g. 2244,2542,63423")
            sys.exit()
    fnargs["cidlist"] = [x.strip(" ") for x in clist]
elif args.cidfile:
    source_file = args.cidfile
    fnargs["file_path"] = args.cidfile
    fnargs["fn"] = fn  # cidfiletoexcel needs to be passed the function to run. Otherwise
    # we can run it directly
else:
    print("you must specify either a file containing CIDs or a comma-delimited list of CIDs on the commmand line")
    sys.exit()


if args.dlall == 1:
    fnargs["download_all"] = True
if args.batchsize:
    fnargs["batch_size"] = args.batchsize

if args.cidlist:
    fn(**fnargs)
else:
    bfc.cidfiletoexcel(**fnargs)